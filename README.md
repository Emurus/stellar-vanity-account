# Stellar Vanity Account
Use this java program to generate a Stellar Account the ends with the name you provide.

## How to use
* Install Java 8
* Run `node generate [name]`
* Note: To avoid very long waiting times, I recommend using a name of max 4 characters
* Note: NEVER EVER share your secret key (starting with the letter S) with anyone

## Example
Run: `java -jar ./Stellar-Account-Vanity`
```
Result:
Found Stellar keypair in 2816 tries
public key: GC...XLM
secret key: S...SLY
```
As you can see the puclic key ends with the provided name: XLM.

## Donations
If you like this script. A small donation will be appreciated. You can make a donation in Stellar Lumens to: `GAPICAFFV3ALBGNFHCCVXG7WPJSXC7RL5BF7HR356334MLAX4U67JAAP`

## License
MIT License

Copyright (c) 2018 Matthew Jarvis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.